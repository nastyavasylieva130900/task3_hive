CREATE DATABASE test_shema;
CREATE DATABASE test_shema2
	LOCATION '/user/my_db';
SHOW DATABASES;
SHOW DATABASES LIKE '*2*';
DROP DATABASE IF EXISTS test_shema2;

USE test_shema;
CREATE TABLE IF NOT EXISTS table_1(
	id int,
	name String);
INSERT INTO TABlE table_1 VALUES (1,'Nastya'), (2,'Nastya2');
SELECT * FROM table_1;

ALTER TABLE table_1 ADD COLUMNS (type int);
SELECT * FROM table_1;
INSERT INTO TABLE table_1
	VALUES (3, 'Nastya3', 2);
SELECT * FROM table_1;


CREATE TABLE IF NOT EXISTS table_b_1(
	id int,
	name String,
	type int)
CLUSTERED BY (type) INTO 6 BUCKETS;

INSERT OVERWRITE TABLE table_b_1
	SELECT id, name, type FROM table_1;

SELECT DISTINCT FILE FROM (SELECT *, INPUT__FILE__NAME AS FILE FROM table_b_1) t;

ALTER TABLE table_b_1 SET TBLPROPERTIES('EXTERNAL'='TRUE');
DESC FORMATTED table_b_1;
DROP TABLE IF EXISTS table_b_1;
DROP TABLE IF EXISTS table_1;

CREATE EXTERNAL TABLE IF NOT EXISTS table_2(
	id int, 
	type int,
	name String);
LOAD DATA INPATH '/warehouse/tablespace/managed/hive/test_shema.db/table_1' 
INTO TABLE table_2;

SELECT * FROM table_2;

CREATE TABLE IF NOT EXISTS table_2_p AS
SELECT * FROM table_2;
DROP TABLE table_2;
CREATE TABLE table_2  (
	id int,
	name String) 
PARTITIONED BY (type int);
INSERT OVERWRITE TABLE table_2 PARTITION(type) 
SELECT * FROM table_2_p;

ALTER TABLE table_2 ADD COLUMNS (action_time date);
SELECT * FROM table_2;

INSERT INTO table_2
VALUES(4, 'Nastya4', 2, '2018-10-31'), (5, 'Nastya5', 3, '2019-11-01');
SELECT * FROM table_2;

SELECT * FROM table_2 
	ORDER BY action_time;

SELECT * FROM table_2
	WHERE action_time > current_date();

SELECT * FROM table_2 
	WHERE action_time > '2018-12-31' and action_time < '2020-01-01';

SHOW PARTITIONS test_shema.table_2;

ALTER TABLE table_2 DROP IF EXISTS PARTITION (type != '2');

SELECT * FROM table_2;

CREATE TABLE IF NOT EXISTS table_3 AS
	SELECT * FROM table_2
	LOCATION '/user/my_bd'
STORED AS ORC tblproperties ('orc.compress' = 'SNAPPY'); 

SELECT * FROM table_3;

SHOW TABLES IN test_shema;
SHOW TABLES IN test_shema LIKE '*3*';
DROP DATABASE IF EXISTS test_shema CASCADE;















